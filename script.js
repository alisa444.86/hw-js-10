const inputFirst = document.querySelector('.input-first');
const inputSecond = document.querySelector('.input-second');
const icons = document.querySelectorAll('.icon-password');
const submit = document.querySelector('.btn');
const warning = document.createElement('div');

icons.forEach(icon => {
    icon.addEventListener('click', () => {
        if (icon.classList.contains('fa-eye')) {
            icon.classList.remove('fa-eye');
            icon.classList.add('fa-eye-slash');
            icon.previousElementSibling.type = 'text';
        } else {
            icon.classList.remove('fa-eye-slash');
            icon.classList.add('fa-eye');
            icon.previousElementSibling.type = 'password';
        }
    })
});

submit.addEventListener('click', (event) => {
    event.preventDefault();
    if (warning) {
        warning.remove();
    }
    let password = inputFirst.value;
    let passwordTwo = inputSecond.value;

    if (password === passwordTwo) {
        alert('You are welcome!');
    } else {
        warning.innerText = 'Нужно ввести одинаковые значения';
        warning.style.color = 'red';
        submit.after(warning);
    }
});


